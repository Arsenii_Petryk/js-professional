module.exports = class CalendarsListObject {
  constructor() {
    this.IsEmpty = element.all(by.cssContainingText('h5', '(Calendar is empty)'));
    this.newCalendarInput = element(by.model('$ctrl.newCalendar'));
    this.addCalendarButton = element(by.css('.add-calendar-button'));
    this.calendarCheckBoxes = element.all(by.model('calendar.checked'));
    this.calendarsNames = element.all(by.binding('calendar.calendar'));
    this.changeNameFields = element.all(by.model('$ctrl.newCalendarName'));
    this.editCalendarButtons = element.all(by.css('.edit-calendar-button'));
    this.deleteCalendarButtons = element.all(by.css('.trash-calendar-button'));
    this.eventsHighliters = element.all(by.binding('$ctrl.highLiter(calendar, event)"'))
    this.eventsTitles = element.all(by.binding('event.title'));
    this.eventsDates = element.all(by.binding('event.date'));
  }

  addCalendar(name) {
    this.addCalendarButton.click();
    this.newCalendarInput.sendKeys(name);
    this.addCalendarButton.click();
  }

  renameCalendar(name) {
    this.editCalendarButtons.last().click();
    this.changeNameFields.last().clear().sendKeys(name);
    this.editCalendarButtons.last().click();
  }

  deleteCalendar() {
    this.deleteCalendarButtons.last().click();
    browser.switchTo().alert().accept();
  }
}
