exports.config = {
  exclude: [],
  baseURL: 'http://localhost:8080',
  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  multiCapabilities: [{
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 3
  }],
  allScriptsTimeout: 11000,
  getPageTimeout: 10000,
  framework: 'jasmine',
  specs: ['./*.spec.js'],
  jasmineNodeOpts: {
    isVerbose: false,
    showColors: true,
    includeStackTrace: false,
    defaultTimeoutInterval: 40000
  }
};
