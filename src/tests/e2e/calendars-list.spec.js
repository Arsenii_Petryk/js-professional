const jsonServer = require('json-server');
const CalendarsListObject = require('./page-objects/calendars-listObject');
describe('Calendar list component', () => {
  const server = jsonServer.create();
  const router = jsonServer.router('db.json');
  const middlewares = jsonServer.defaults();
  server.use(middlewares);
  server.use(router);
  server.listen(3000, () => {
    console.log('JSON Server is running');
  });

  browser.get('http://localhost:8080/');
  const calendarList = new CalendarsListObject();

  it('should create new calendar', () => {
    calendarList.addCalendar('Summer calendar');
    expect(calendarList.calendarsNames.getText()).toContain('Summer calendar');
  });

  it('should check empty lable of new calendar', () => {
    expect(calendarList.IsEmpty.last().isDisplayed()).toBeTruthy();
  });

  it('should edit calendar name', () => {
    calendarList.renameCalendar('Winter calendar');
    expect(calendarList.calendarsNames.getText()).toContain('Winter calendar');
  });

  it('should delete calendar', () => {
    calendarList.deleteCalendar();
    expect(calendarList.calendarsNames.getText()).not.toContain('Winter calendar');
  });
});
