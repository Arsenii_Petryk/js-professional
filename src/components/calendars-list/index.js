import ng from 'angular';

import CalendarsListComponent from './component';

export default ng.module('app.components.calendarsList', [])
  .component('calendarsList', CalendarsListComponent)
  .name;
