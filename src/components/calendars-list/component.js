import './calendars-list.css';
import template from './calendars-list.html';
import controller from './controller';

export default {
  template,
  controller,
  require: {
    calendarBar: '^^calendarBar'
  },
  bindings: {
    events: '='
  }
};
