export default class CalendarsListController {

  highLiter(calendar, event) {
    if (calendar.checked) {
      return event.status;
    }
    return '';
  }

  createCalendar() {
    this.newCalendar && this.calendarBar.createCalendar(this.newCalendar);
    this.newCalendar = undefined;
  }

  edit(id) {
    this.newCalendarName && this.calendarBar.updateCalendar(this.newCalendarName, id);
    this.newCalendarName = undefined;
  }

  remove(id) {
    confirm('are you shure?') && this.calendarBar.deleteCalendar(id);
  }

  setSelection(date) {
    this.calendarBar.updateSelected(new Date(date));
  }
}
