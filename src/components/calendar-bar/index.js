import ng from 'angular';

import CalendarBarComponent from './component';
import DateService from './service';

export default ng.module('app.components.calendarBar', [])
  .service('DateService', DateService)
  .component('calendarBar', CalendarBarComponent)
  .name;
