import './calendar-bar.css';
import template from './calendar-bar.html';
import controller from './controller';

export default {
  template,
  controller
};
