import ng from 'angular';

export default class CalendarBar {
  constructor(DateService) {
    'ngInject';
    this.dServ = DateService;
    this.events = DateService.getCurrentEvents();
    this.calendars = DateService.getCurrentCalendars();
    this.calendarsArr = DateService.getCurrentCalendarsArr();
    this.selected = new Date();
    this.callbacksToRefresh = [this.updateCurrents.bind(this)];
  }

  $onInit() {
    this.dServ.getAll()
      .then(() => this.updateAll());
  }

  updateAll() {
    this.updateCurrents();
    this.refreshDP();
  }

  refresh(date) {
    this.callbacksToRefresh.forEach((func) => {
      func(date);
    });
  }

  updateSelected(date) {
    this.selected = date;
    this.refresh(date);
  }

  updateCurrents() {
    this.updCurrEvent();
    this.updCurrCalendar();
  }

  updCurrEvent() {
    const dayToCheck = new Date(this.selected).setHours(0, 0, 0, 0);
    this.currEvent = ng.copy(this.events.find(
      event => dayToCheck === new Date(event.date).setHours(0, 0, 0, 0)
    ));
  }

  updCurrCalendar() {
    if (this.currEvent) {
      this.currCalendar = ng.copy(this.calendarsArr.find(
        calendar => calendar.events.some(
          id => id === this.currEvent.id
        )
      ));
    }
  }

  refreshDP() {
    this.selected = new Date(this.selected);
  }

  removeEvent(id) {
    this.dServ.removeEventUpdateCalendar(id)
      .then(() => this.updateAll());
  }

  addEvent(event, calendar) {
    this.dServ.addEventUpdateCalendar(event, calendar)
      .then(() => this.updateAll());
  }

  editEvent(event) {
    this.dServ.updateEvent(event)
      .then(() => this.updateAll());
  }

  createCalendar(calendar) {
    this.dServ.createCalendar(calendar)
      .then(() => this.updateAll());
  }

  deleteCalendar(id) {
    this.dServ.deleteCalendarWithEvents(id)
      .then(() => this.updateAll());
  }

  updateCalendar(name, id) {
    const calendar = this.calendarsArr.find(cl => cl.id === id);
    calendar.calendar = name;
    this.dServ.updateCalendar(calendar)
      .then(() => this.updateAll());
  }
}
