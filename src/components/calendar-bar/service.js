import ng from 'angular';

export default class DateService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
    this.events = [];
    this.calendars = [];
    this.calendArr = [];
  }

  getAll() {
    return this.getCalendars()
      .then(calendArr => {
        this.refrashThisArr(this.calendArr, calendArr);
      })
      .then(() => this.getEvents())
      .then(events => {
        this.refrashThisArr(this.events, events);
      })
      .then(() => this.convert(this.calendArr, this.events))
      .then(calendars => {
        this.refrashThisArr(this.calendars, calendars);
      });
  }


  removeEventUpdateCalendar(id) {
    return this.deleteEvent(id)
      .then(() => {
        const calendar = this.getEventCalendarArr(id);
        calendar.events.splice(this.ind(calendar.events, id), 1);
        return this.updateCalendar(calendar);
      });
  }

  addEventUpdateCalendar(event, calendar) {
    return this.createEvent(event)
      .then(newEv => {
        calendar.events.push(newEv.id);
        return this.updateCalendar(calendar);
      });
  }

  deleteCalendarWithEvents(id) {
    const calendar = this.calendArr.find(cl => cl.id === id);
    return this.deleteCalendar(id)
      .then(() => calendar.events.length &&
        Promise.all(calendar.events.map(evId => this.deleteEvent(evId)))
      );
  }

  createCalendar(calendar) {
    return this.makeHttpRequests({
      method: 'post',
      search: 'calendars',
      data: {
        calendar,
        checked: true,
        id: (new Date()).getTime(),
        events: []
      }
    }).then(cl => ['calendars', 'calendArr'].forEach(arr => this[arr].push(cl)));
  }

  updateCalendar(calendar) {
    let bufCalendar;
    return this.makeHttpRequests({
      method: 'put',
      search: 'calendars',
      id: calendar.id,
      data: calendar
    })
      .then(newCalendar => (bufCalendar = newCalendar) && this.ind(this.calendArr, newCalendar.id))
      .then(index => (this.calendArr[index] = bufCalendar))
      .then(() => this.convert(this.calendArr, this.events))
      .then(conv => this.refrashThisArr(this.calendars, conv) && bufCalendar);
  }

  updateEvent(event) {
    return this.makeHttpRequests({
      method: 'put',
      search: 'events',
      id: event.id,
      data: event
    })
      .then(ev => this.events.splice(this.ind(this.events, ev.id), 1, ev) && ev)
      .then(ev => {
        const calendar = this.getEventCalendar(ev.id);
        calendar.events.splice(this.ind(calendar.events, ev.id), 1, ev);
      });
  }

  deleteCalendar(id) {
    return this.makeHttpRequests({
      method: 'delete',
      search: 'calendars',
      id
    })
      .then(() => {
        const index = this.ind(this.calendars, id);
        ['calendars', 'calendArr'].forEach(arr => this[arr].splice(index, 1));
      });
  }

  deleteEvent(id) {
    return this.makeHttpRequests({
      method: 'delete',
      search: 'events',
      id
    })
      .then(() => this.events.splice(this.ind(this.events, id), 1) && id);
  }

  createEvent(event) {
    event.id = (new Date()).getTime();
    return this.makeHttpRequests({
      method: 'post',
      search: 'events',
      data: event
    })
      .then(ev => this.events.push(ev) && ev);
  }

  makeHttpRequests({
    method,
    search,
    id,
    data
  }) {
    return this.$http[method](
        (id) ? `${URL}/${search}/${id}` :
        `${URL}/${search}`, data)
      .then(response => response && response.data, err => console.log(err));
  }

  convert(calendArr, events) {
    const calendars = ng.copy(calendArr);
    calendars.forEach(calendar => {
      calendar.events = calendar.events.map(
        id => events.find(event => event.id === id)
      );
    });
    return calendars;
  }

  ind(arr, id) {
    return arr.indexOf(arr.find(obj => obj.id === id));
  }

  refrashThisArr(thisArr, newArr) {
    thisArr.length = 0;
    newArr.forEach(obj => thisArr.push(obj));
  }

  getEventCalendar(id) {
    return this.calendars.find(cl => cl.events.some(ev => ev.id === id));
  }

  getEventCalendarArr(id) {
    return this.calendArr.find(cl => !!~cl.events.indexOf(id));
  }

  getCalendar(id) {
    return this.makeHttpRequests({
      method: 'get',
      search: 'calendars',
      id
    });
  }

  getCalendars() {
    return this.makeHttpRequests({
      method: 'get',
      search: 'calendars'
    });
  }

  getEvent(id) {
    return this.makeHttpRequests({
      method: 'get',
      search: 'events',
      id
    });
  }

  getEvents() {
    return this.makeHttpRequests({
      method: 'get',
      search: 'events'
    });
  }

  getCurrentCalendars() {
    return this.calendars;
  }

  getCurrentEvents() {
    return this.events;
  }

  getCurrentCalendarsArr() {
    return this.calendArr;
  }
}
