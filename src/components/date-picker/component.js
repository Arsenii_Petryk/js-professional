import './date-picker.css';
import template from './date-picker.html';
import controller from './controller';

export default {
  template,
  controller,
  require: {
    calendarBar: '^^calendarBar'
  },
  bindings: {
    events: '=',
    selected: '='
  }
};
