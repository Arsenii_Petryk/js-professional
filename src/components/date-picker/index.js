import ng from 'angular';

import DatePickerComponent from './component';

export default ng.module('app.components.datePicker', [])
  .component('datepicker', DatePickerComponent)
  .name;
