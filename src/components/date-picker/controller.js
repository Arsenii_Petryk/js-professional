export default class dpController {
  constructor() {
    const events = this.events;
    function getDayClass(data) {
      const date = data.date;
      const mode = data.mode;
      if (mode === 'day') {
        const dayToCheck = new Date(date).setHours(0, 0, 0, 0);
        for (let i = 0; i < events.length; i++) {
          if (events[i].checked) {
            for (let n = 0; n < events[i].events.length; n++) {
              const currentDay = new Date(events[i].events[n].date).setHours(0, 0, 0, 0);
              if (dayToCheck === currentDay) {
                return events[i].events[n].status;
              }
            }
          }
        }
      }
      return '';
    }

    this.options = {
      customClass: getDayClass,
      minDate: null,
      showWeeks: false,
    };
  }

  refreshDP(day) {
    return new Date(day.getTime());
  }
}
