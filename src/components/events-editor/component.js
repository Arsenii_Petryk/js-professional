import './events-editor.css';
import template from './events-editor.html';
import controller from './controller';

export default {
  template,
  controller,
  require: {
    calendarBar: '^^calendarBar'
  },
  bindings: {
    event: '=',
    selected: '=',
    callbacks: '=',
    calendars: '=',
    currentCalendar: '='
  }
};
