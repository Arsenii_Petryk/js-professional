import ng from 'angular';

import EventsEditorComponent from './component';

export default ng.module('app.components.eventsEditor', [])
  .component('eventsEditor', EventsEditorComponent)
  .name;
