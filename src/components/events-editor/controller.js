export default class EvensEditorController {
  constructor() {
    this.callbacks.push(this.show.bind(this));
  }

  valid() {
    const validation = ['status', 'title', 'description'];
    return this.event && this.currentCalendar
                      && validation.every(i => !!this.event[i]);
  }

  add() {
    this.calendarBar.addEvent(this.createEvent(), this.currentCalendar);
  }

  edit() {
    this.valid() && this.calendarBar.editEvent(this.event);
  }

  createEvent() {
    this.event.date = this.selected;
    return this.event;
  }

  show() {
    this.editBlock = false;
    this.currentCalendar = undefined;
  }

  remove(id) {
    confirm('Are you shure?') && this.calendarBar.removeEvent(id);
    this.currentCalendar = undefined;
  }

  active(status) {
    if (this.event && this.event.status === status) return 'active selected';
    return '';
  }
}
