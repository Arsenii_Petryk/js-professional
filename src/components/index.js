import ng from 'angular';

import CalendarBar from './calendar-bar';
import CalendarList from './calendars-list';
import DatePicker from './date-picker';
import EventEditor from './events-editor';

export default ng.module('app.components',
  [
    CalendarBar,
    CalendarList,
    DatePicker,
    EventEditor
  ]
).name;
