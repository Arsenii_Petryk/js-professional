import 'bootstrap/dist/css/bootstrap.css';
import './style.css';

import ng from 'angular';
import UIB from 'angular-ui-bootstrap';
import Components from './components';

ng.module('app', [UIB, Components]);
